// $(window).scroll(function(){
//     $('nav').removeClass('navbar-first');
//     $('nav').addClass('navbar-scroll');
// });

$(window).on('scroll', function() {
    var scrollTop = $(this).scrollTop();
    if (scrollTop + $(this).innerHeight() >= this.scrollHeight) {
      console.log('end reached');
    } else if (scrollTop <= 0) {
        $('nav').addClass('navbar-first');
        $('nav').removeClass('navbar-dark');
        $('nav').removeClass('navbar-scroll');
    } else {
        $('nav').removeClass('navbar-first');
        $('nav').addClass('navbar-scroll');
        $('nav').addClass('navbar-dark');
    }
  });